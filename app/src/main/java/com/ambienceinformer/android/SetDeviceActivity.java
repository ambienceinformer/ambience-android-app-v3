package com.ambienceinformer.android;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ambienceinformer.android.Utils.PreferenceManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SetDeviceActivity extends AppCompatActivity {

    @BindView(R.id.btnSet)
    Button btnSet;

    @BindView(R.id.etDeviceId)
    EditText etDeviceId;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    FirebaseFirestore db = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_device);
        ButterKnife.bind(this);

        StatusBarUtil.setTransparent(this);
        db = FirebaseFirestore.getInstance();
    }

    public void setDevice(View view) {
        pbLoading.setVisibility(View.VISIBLE);
        final String deviceId = etDeviceId.getText().toString();
        if (deviceId != null && !deviceId.equals("")) {
            db.collection("weather_info").document(deviceId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot snapshot = task.getResult();
                        if (snapshot != null && snapshot.exists()) {
                            pbLoading.setVisibility(View.INVISIBLE);
                            PreferenceManager.getInstance().put(Constants.DEVICE_ID, deviceId);
                            PreferenceManager.getInstance().put(Constants.IS_FIRST_RUN, false);
                            startActivity(new Intent(SetDeviceActivity.this, HomeActivity.class));
                            finish();
                        } else {
                            pbLoading.setVisibility(View.INVISIBLE);
                            Toast.makeText(SetDeviceActivity.this, "No registered devices Found!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        } else {
            Toast.makeText(this, "Please enter device Id!", Toast.LENGTH_SHORT).show();
            pbLoading.setVisibility(View.INVISIBLE);
        }

    }

    public void registerDevice(View view) {
        startActivity(new Intent(this, RegisterDeviceActivity.class));
        finish();
    }
}
