package com.ambienceinformer.android;

import android.app.Application;

import com.ambienceinformer.android.Utils.PreferenceManager;

/**
 * Created by Believe on 18-03-2018.
 */

public class AmbienceApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.createInstance(this, PreferenceManager.WRITE_SYNC);
    }
}
