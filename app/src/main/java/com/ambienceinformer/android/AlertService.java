package com.ambienceinformer.android;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.renderscript.RenderScript;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ambienceinformer.android.Utils.PreferenceManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

/**
 * Created by hasui on 20-03-2018.
 */

public class AlertService extends Service {

    private static final String TAG = "Alert Service";
    private FirebaseFirestore db = null;
    private Boolean isGreaterThan;
    private long alertValue;
    private String alertKeyType;
    private String deviceId;
    private boolean isNotified = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand: ");
        db = FirebaseFirestore.getInstance();
        if (intent != null) {
            Bundle alertSetting = intent.getBundleExtra(Constants.ALERT_BUNDLE);
            initService(alertSetting);
        } else {
            Log.e(TAG, "onStartCommand: stopped service");
            stopSelf();
        }
        return START_REDELIVER_INTENT;
    }

    private void initService(final Bundle alertSetting) {
        deviceId = PreferenceManager.getInstance().get(Constants.DEVICE_ID, "");
        if (!deviceId.equals("")) {
            isGreaterThan = alertSetting.getBoolean(Constants.GREATER_THAN_FLAG);
            alertValue = alertSetting.getLong(Constants.ALERT_VALUE);
            alertKeyType = alertSetting.getString(Constants.ALERT_TYPE_KEY);
            getData();
        } else {
            Log.e(TAG, "onStartCommand: stopped service");
            stopSelf();
        }
    }

    private void getData() {
        db.collection("weather_info").document(String.valueOf(deviceId)).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    Log.e(TAG, "onComplete: Successfully got data");
                    DocumentSnapshot documentSnapshot = task.getResult();
                    if (documentSnapshot != null && documentSnapshot.exists()) {
                        long currentValue = (long) documentSnapshot
                                .getData()
                                .get(alertKeyType);
                        compareValue(isGreaterThan, currentValue, alertValue, alertKeyType);
                    }
                } else {
                    Log.e(TAG, "onComplete: Failed to get data!");
                }
            }
        });
    }

    private void compareValue(Boolean isGreaterThan, long currentValue, long alertValue, String alertType) {
        if (isGreaterThan) {
            if (currentValue > alertValue) {
                Log.e(TAG, "compareValue: is greater than");
                creatAlert("greater than", alertType, alertValue);
            }
        } else {
            if (currentValue < alertValue) {
                Log.e(TAG, "compareValue: is less than");
                creatAlert("less than", alertType, alertValue);
            }
        }
        if (!isNotified) {
            getData();
        }
        Log.e(TAG, "compareValue: requesting again!");
    }

    private void creatAlert(String str, String alertType, long alertValue) {

        Intent intent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 7, intent, PendingIntent.FLAG_ONE_SHOT);

        Notification notification = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Ambience Alert!")
                .setContentText(alertType + " is " + str + " " + alertValue)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .build();

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.notify((int) System.currentTimeMillis(), notification);
        Log.e(TAG, "creatAlert: Alert Created and notified");
        isNotified = true;
        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
