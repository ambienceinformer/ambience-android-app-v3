package com.ambienceinformer.android;

/**
 * Created by hasui on 20-03-2018.
 */

public class Device {
    private String deviceId;
    private String temperature;
    private String humidity;
    private String airPressure;
    private String location;
    private String battery;
    private String rainStatus;

    public Device(String deviceId, String temperature, String humidity, String airPressure, String location, String battery, String rainStatus) {
        this.deviceId = deviceId;
        this.temperature = temperature;
        this.humidity = humidity;
        this.airPressure = airPressure;
        this.location = location;
        this.battery = battery;
        this.rainStatus = rainStatus;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getAirPressure() {
        return airPressure;
    }

    public String getLocation() {
        return location;
    }

    public String getBattery() {
        return battery;
    }

    public String getRainStatus() {
        return rainStatus;
    }
}
