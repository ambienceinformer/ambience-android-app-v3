package com.ambienceinformer.android;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ambienceinformer.android.Utils.PreferenceManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeLocationActivity extends AppCompatActivity {

    @BindView(R.id.etLocation)
    EditText etLocation;

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_location);
        ButterKnife.bind(this);

        StatusBarUtil.setTransparent(this);
        db = FirebaseFirestore.getInstance();
    }

    public void changeLocation(View view) {
        pbLoading.setVisibility(View.VISIBLE);
        final String deviceId = PreferenceManager.getInstance().get(Constants.DEVICE_ID, "");
        final String location = etLocation.getText().toString();
        if (deviceId != null && !deviceId.equals("") && location != null && !location.equals("")) {
            DocumentReference docRef = db.collection("weather_info").document(deviceId);
            if (docRef != null) {
                Map<String, Object> device = new HashMap<>();
                device.put(Constants.LOCATION, location);
                docRef.update(device).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            startActivity(new Intent(ChangeLocationActivity.this, HomeActivity.class));
                        } else {
                            Toast.makeText(ChangeLocationActivity.this, "Unable to update location!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else {
                Toast.makeText(ChangeLocationActivity.this, "Invalid Device ID", Toast.LENGTH_SHORT).show();
                pbLoading.setVisibility(View.INVISIBLE);
            }
        } else {
            Toast.makeText(this, "Please fill all fields!", Toast.LENGTH_SHORT).show();
            pbLoading.setVisibility(View.INVISIBLE);
        }
    }
}
