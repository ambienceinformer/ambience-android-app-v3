package com.ambienceinformer.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WelcomeScreenActivity extends AppCompatActivity {

    @BindView(R.id.vpIntroSlider)
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);
        ButterKnife.bind(this);
        startActivity(new Intent(this,SetDeviceActivity.class));
    }

}
