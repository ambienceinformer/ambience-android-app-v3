package com.ambienceinformer.android;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ambienceinformer.android.Utils.PreferenceManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.jaeger.library.StatusBarUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterDeviceActivity extends AppCompatActivity {

    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    @BindView(R.id.etDeviceId)
    EditText etDeviceId;

    @BindView(R.id.etLocation)
    EditText etLocation;

    private FirebaseFirestore db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_device);
        ButterKnife.bind(this);

        StatusBarUtil.setTransparent(this);
        db = FirebaseFirestore.getInstance();
    }

    public void setDevice(View view) {
        startActivity(new Intent(this, SetDeviceActivity.class));
        finish();
    }

    public void registerDevice(View view) {
        pbLoading.setVisibility(View.VISIBLE);
        final String deviceId = etDeviceId.getText().toString();
        final String location = etLocation.getText().toString();
        if (deviceId != null && !deviceId.equals("") && location != null && !location.equals("")) {
            db.collection("users").document("active_users").get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {
                        DocumentSnapshot snapshot = task.getResult();
                        if (snapshot != null && snapshot.exists()) {
                            Map<String, Object> data = snapshot.getData();
                            ArrayList<String> list = (ArrayList<String>) data.get("ids");
                            if (list.contains(Long.parseLong(deviceId))) {
                                register(deviceId, location);
                            }
                        }
                    }
                }
            });
        } else {
            Toast.makeText(this, "Please fill all fields!", Toast.LENGTH_SHORT).show();
            pbLoading.setVisibility(View.INVISIBLE);
        }
    }

    private void register(final String deviceId, String location) {
        Map<String, Object> newDevice = new HashMap<String, Object>();
        newDevice.put(Constants.DEVICE_ID, deviceId);
        newDevice.put(Constants.LOCATION, location);
        newDevice.put(Constants.RAIN_STATUS, "N/A");
        newDevice.put(Constants.BATTERY, "N/A");
        newDevice.put(Constants.AIR_PRESSURE, "N/A");
        newDevice.put(Constants.TEMPERATURE, "N/A");
        newDevice.put(Constants.HUMIDITY, "N/A");
        db.collection("weather_info").document(deviceId).set(newDevice).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    pbLoading.setVisibility(View.INVISIBLE);
                    PreferenceManager.getInstance().put(Constants.DEVICE_ID, deviceId);
                    PreferenceManager.getInstance().put(Constants.IS_FIRST_RUN, false);
                    startActivity(new Intent(RegisterDeviceActivity.this, HomeActivity.class));
                    finish();
                } else {
                    pbLoading.setVisibility(View.INVISIBLE);
                    Toast.makeText(RegisterDeviceActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
