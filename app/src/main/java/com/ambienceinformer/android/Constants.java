package com.ambienceinformer.android;

/**
 * Created by hasui on 19-03-2018.
 */

public class Constants {

    public static final String IS_FIRST_RUN = "firstRun";
    public static final String DEVICE_ID = "deviceId";
    public static final String TEMPERATURE = "temperature";
    public static final String HUMIDITY = "humidity";
    public static final String AIR_PRESSURE = "airPressure";
    public static final String BATTERY = "battery";
    public static final String RAIN_STATUS = "rainStatus";
    public static final String LOCATION = "location";
    public static final String ALERT_BUNDLE = "alert_settings";
    public static final String ALERT_TYPE_KEY = "alertType";
    public static final String ALERT_VALUE="value";
    public static final String GREATER_THAN_FLAG="isGreatherThan";
}
