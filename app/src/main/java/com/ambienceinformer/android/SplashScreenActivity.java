package com.ambienceinformer.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ambienceinformer.android.Utils.PreferenceManager;

import butterknife.ButterKnife;

public class SplashScreenActivity extends AppCompatActivity {

    PreferenceManager mPrefManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        ButterKnife.bind(this);
        mPrefManager = PreferenceManager.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent;
        if (!mPrefManager.contains("firstRun")) {
            intent = new Intent(this, WelcomeScreenActivity.class);
        } else {
            intent = new Intent(this, HomeActivity.class);
        }
        showSplash(intent);
    }

    private void showSplash(final Intent intent) {
        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startActivity(intent);
                    finish();
                }
            }
        };
        timerThread.start();
    }
}
