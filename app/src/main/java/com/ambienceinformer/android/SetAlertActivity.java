package com.ambienceinformer.android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.jaeger.library.StatusBarUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SetAlertActivity extends AppCompatActivity {

    @BindView(R.id.etTemperature)
    EditText etTemperature;

    @BindView(R.id.rbGreaterThan)
    RadioButton rbGreaterThan;

    @BindView(R.id.rbLessThan)
    RadioButton rbLessThan;
    private String TAG = "Set Alert Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_alert);
        ButterKnife.bind(this);

        StatusBarUtil.setTransparent(this);
    }

    public void setAlert(View view) {
        String temp = etTemperature.getText().toString();
        if (temp != null && !temp.equals("")) {
            Boolean isGreaterThan = rbGreaterThan.isChecked();
            long alertValue = Long.parseLong(temp);
            StartAlertService(alertValue, isGreaterThan, Constants.TEMPERATURE);
        } else {
            Toast.makeText(this, "Please enter a value", Toast.LENGTH_SHORT).show();
        }
    }

    private void StartAlertService(long value, Boolean isGreaterThan, String type) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.ALERT_TYPE_KEY, type);
        bundle.putLong(Constants.ALERT_VALUE, value);
        bundle.putBoolean(Constants.GREATER_THAN_FLAG, isGreaterThan);

        Intent intent = new Intent(this, AlertService.class);
        intent.putExtra(Constants.ALERT_BUNDLE, bundle);
        startService(intent);
        Toast.makeText(this, "Alert is set.", Toast.LENGTH_SHORT).show();
        finish();
    }
}
