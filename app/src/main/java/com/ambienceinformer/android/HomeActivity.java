package com.ambienceinformer.android;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.ambienceinformer.android.Utils.PreferenceManager;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.jaeger.library.StatusBarUtil;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tvTemperature)
    TextView tvTemperature;

    @BindView(R.id.tvLocation)
    TextView tvLocation;

    @BindView(R.id.tvDeviceId)
    TextView tvDeviceId;

    @BindView(R.id.tvHumidity)
    TextView tvHumidity;

    @BindView(R.id.tvAirPressure)
    TextView tvAirPressure;

    @BindView(R.id.tvBattery)
    TextView tvBattery;

    @BindView(R.id.tvRainStatus)
    TextView tvRainStatus;

    @BindView(R.id.drawerLayout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.navigationView)
    NavigationView mNavigationView;

    PreferenceManager mPrefManager;
    private String deviceId;

    FirebaseFirestore db = null;
    private String TAG = "Home Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);
        setDrawer();
        StatusBarUtil.setTransparent(this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getWindow(); // in Activity's onCreate() for instance
//            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//        }
        db = FirebaseFirestore.getInstance();

        mPrefManager = PreferenceManager.getInstance();
    }

    @Override
    protected void onResume() {
        if (mPrefManager.contains(Constants.DEVICE_ID)) {
            deviceId = mPrefManager.get(Constants.DEVICE_ID, "0");
            if (!deviceId.equals("")) {
                setDevice(deviceId);
            } else {
                startActivity(new Intent(this, SetDeviceActivity.class));
                finish();
            }
        }
        super.onResume();
    }

    private void setDrawer() {
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.open, R.string.close);
        actionBarDrawerToggle.syncState();
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void setDevice(final String deviceId) {
        db.collection("weather_info").document(deviceId).addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if (documentSnapshot != null && documentSnapshot.exists()) {
                    Log.e(TAG, "onEvent: data= " + documentSnapshot.getData());
                    Map<String, Object> data = documentSnapshot.getData();
                    String temperature = String.valueOf(data.get(Constants.TEMPERATURE));
                    String humidity = String.valueOf(data.get(Constants.HUMIDITY));
                    String airPressure = String.valueOf(data.get(Constants.AIR_PRESSURE));
                    String battery = String.valueOf(data.get(Constants.BATTERY));
                    String location = String.valueOf(data.get(Constants.LOCATION));
                    String rainStatus = String.valueOf(data.get(Constants.RAIN_STATUS));
                    Device device = new Device(deviceId, temperature, humidity, airPressure, location, battery, getRainStatusString(rainStatus));
                    setReceivedData(device);
                } else {
                    Log.e(TAG, "Current data: null");
                }
            }
        });
    }

    private void setReceivedData(Device device) {
        tvDeviceId.setText(device.getDeviceId());
        tvLocation.setText(device.getLocation());
        tvTemperature.setText(device.getTemperature());
        tvAirPressure.setText(device.getAirPressure());
        tvHumidity.setText(device.getHumidity());
        tvBattery.setText(device.getBattery());
        tvRainStatus.setText(device.getRainStatus());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.navSetDevice:
                startActivity(new Intent(this, SetDeviceActivity.class));
                break;
            case R.id.navRegisterDevice:
                startActivity(new Intent(this, RegisterDeviceActivity.class));
                break;
            case R.id.navSetAlert:
                startActivity(new Intent(this, SetAlertActivity.class));
                break;
            case R.id.navChnageLocation:
                startActivity(new Intent(this, ChangeLocationActivity.class));
                break;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    private String getRainStatusString(String rainStatus) {
        if(rainStatus.equals("N/A"))
        {
            return "N/A";
        }
        int value = Integer.parseInt(rainStatus);
        if (value <= 300) return "No Rain";
        else if (value >= 301 && value <= 550) return "Sparkling";
        else if (value > 550 && value <= 700) return "Low Raining";
        else if (value > 700 && value <= 900) return "Medium Raining";
        else if (value > 900 && value <= 1024) return "Heavy Raining";
        else return "Unknown";
    }
}
